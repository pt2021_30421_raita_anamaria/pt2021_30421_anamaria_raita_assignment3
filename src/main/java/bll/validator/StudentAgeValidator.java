package bll.validator;

import model.Client;

/**
 * This class is used to validate the age of a client which should be between 14 and 100 years old.
 */
public class StudentAgeValidator implements Validator<Client> {
    private static final int MIN_AGE = 14;
    private static final int MAX_AGE = 100;

    /**
     * This method validates the age of a client.
     * @param t-a client
     * @return-1 if it is not valid, 0 if it is valid
     */
    public int validate(Client t) {

        if (t.getAge() < MIN_AGE || t.getAge() > MAX_AGE) {
            System.out.println("age wrong");
           return -1;
        }
        return 0;
    }

}
