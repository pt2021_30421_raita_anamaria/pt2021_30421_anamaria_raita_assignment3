package bll.validator;

import java.util.regex.Pattern;

import model.Client;

/**
 * This class is used in order to implement a name validator for the client.
 */
public class NameValidator implements Validator<Client> {
    private static final String NAME_PATTERN = "[a-zA-Z ]+";

    /**
     * This method validates the e-mail of a client.
     * @param t-a client
     * @return-1 if it is not valid, 0 if it is valid
     */
    public int validate(Client t) {
        Pattern pattern = Pattern.compile(NAME_PATTERN);
        if (!pattern.matcher(t.getName()).matches()) {
            System.out.println("name wrong");
            return -1;
        }
        return 0;
    }

}
