package bll.validator;

public interface Validator<T> {

    public int validate(T t);
}