package bll.validator;

import model.Product;

/**
 * This class is used in order to validate the quantity of an introduced product,
 * which should be positive.
 */
public class ProductQuantity implements Validator<Product> {
    private static final int MIN_q = 1;

    /**
     * This method validates the quantity of a product.
     * @param t-a product
     * @return-1 if it is not valid, 0 if it is valid
     */
    public int validate(Product t) {

        if (t.getQuantity() < MIN_q) {
            return -1;
        }
        return 0;
    }

}