package bll.validator;

import java.util.regex.Pattern;

import model.Product;

/**
 * This method is used in order to validate the name of the product.
 */
public class ProductName implements Validator<Product> {
    private static final String NAME_PATTERN = "[a-zA-Z ]+";

    /**
     * This method validates the name of a product.
     * @param t-a product
     * @return-1 if it is not valid, 0 if it is valid
     */
    public int validate(Product t) {
        Pattern pattern = Pattern.compile(NAME_PATTERN);
        if (!pattern.matcher(t.getName()).matches()) {
            return -1;
        }
        return 0;
    }

}