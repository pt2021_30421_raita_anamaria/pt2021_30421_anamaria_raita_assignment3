package bll.validator;

import model.Product;

/**
 * This class is used to validate the price of a product, which should be a positive value.
 */
public class ProductPrice implements Validator<Product> {
    private static final float MIN_q = 0;

    /**
     * This method validates the introduced price of a product.
     * @param t-a product
     * @return-1 if it is not valid, 0 if it is valid
     */
    public int validate(Product t) {

        if (t.getQuantity() <= MIN_q) {
           return -1;
        }
        return 0;
    }

}