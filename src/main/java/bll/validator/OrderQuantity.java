package bll.validator;

import dao.ProductDAO;
import model.Order;
import model.Product;

/**
 * This class is used in order to validate the introduced quantity of an order.
 */
public class OrderQuantity implements Validator<Order> {
    /**
     * This method is used to validate if the quantity of an order is introduced correctly,
     * if the warehouse has enough stock.
     * @param order-a order
     * @return-1 if it is not valid, 0 if it is valid
     */
    public int validate(Order order) {

        ProductDAO productDAO=new ProductDAO();
        Product pr=productDAO.findById(order.getProduct_id());
        if(order.getQuantity()>pr.getQuantity()){
            return -1;
        }
        return 0;
    }
}
