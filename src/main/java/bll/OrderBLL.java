package bll;

import java.util.ArrayList;
import java.util.List;
import java.util.NoSuchElementException;

import bll.validator.*;
import dao.OrderDAO;
import dao.ProductDAO;
import model.Client;
import model.Order;
import model.Product;
/**
 * This class is used in order to implement all the business logic on an order.
 * It contains all the validations methods for the order.
 * It also contains all the operations we can perform on a order:insert,show all and generate bill.
 */
public class OrderBLL {

    private List<Validator<Order>> validators;
    private OrderDAO orderDAO;
    /**
     * @constructor
     * This constructor initialises the orderDao and the list of validators.
     */
    public OrderBLL() {
        validators = new ArrayList<Validator<Order>>();
        validators.add(new OrderQuantity());
        orderDAO = new OrderDAO();
    }
    /**
     * This method returns the order with a certain id found in the order table.
     * @param id-the id of the order
     * @return an order
     */
    public Order findOrderById(int id) {
        Order st = orderDAO.findById(id);
        if (st == null) {
            throw new NoSuchElementException("The order with id =" + id + " was not found!");
        }
        return st;
    }
    /**
     * This method returns a list with all the orders in the table
     * @return List<Order>
     */
    public List<Order> findAll() {
        List<Order> products= new ArrayList<>();
        products = orderDAO.findAll();
        if (products == null) {
            throw new NoSuchElementException("No elements to show");
        }
        return products;
    }
    /**
     * This method is used in order to introduce an order,which is given as
     * a parameter in the data base if
     * all the info about it is valid.
     * Also, it adjusts the quantity of the product after an order was made.
     * @param cl-a order
     * @return -1 if the order's info is not valid, 0 if it is
     * @throws IllegalAccessException
     */
    public int insert(Order cl) throws IllegalAccessException {
        ProductDAO productDAO=new ProductDAO();
        Product pr= productDAO.findById(cl.getProduct_id());
        for(Validator v: validators)
        {
            if(v.validate(cl)==-1)
            {
                return -1;
            }
        }
        pr.setQuantity(pr.getQuantity()-cl.getQuantity());
        productDAO.update(pr.getId(),pr);
        orderDAO.insert(cl);
        return 0;
    }

}
