package bll;

import java.util.ArrayList;
import java.util.List;
import java.util.NoSuchElementException;

import bll.validator.ProductName;
import bll.validator.ProductPrice;
import bll.validator.ProductQuantity;
import bll.validator.Validator;
import dao.ProductDAO;
import model.Client;
import model.Product;
/**
 * This class is used in order to implement all the business logic on a product.
 * It contains all the validations methods for the product.
 * It also contains all the operations we can perform on a product:insert,delete,update and show all.
 */
public class ProductBLL {

    private List<Validator<Product>> validators;
    private ProductDAO productDAO;

    /**
     * @constructor
     * This constructor initialises the productDao and the list of validators.
     */
    public ProductBLL() {
        validators = new ArrayList<Validator<Product>>();
        validators.add(new ProductName());
        validators.add(new ProductPrice());
        validators.add(new ProductQuantity());
        productDAO = new ProductDAO();
    }

    /**
     * This method returns the product with a certain id found in the product table.
     * @param id-the id of the product
     * @return a product
     */
    public Product findProductById(int id) {
        Product st = productDAO.findById(id);
        if (st == null) {
            throw new NoSuchElementException("The product with id =" + id + " was not found!");
        }
        return st;
    }

    /**
     * This method returns a list with all the products in the table
     * @return List<Product>
     */
    public List<Product> findAll() {
        List<Product> products= new ArrayList<>();
        products = productDAO.findAll();
        if (products == null) {
            throw new NoSuchElementException("No elements to show");
        }
        return products;
    }

    /**
     * This method is used in order to introduce a product,which is given as
     * a parameter in the data base if
     * all the info about it is valid.
     * @param cl-a product
     * @return -1 if the product's info is not valid, 0 if it is
     * @throws IllegalAccessException
     */
    public int insert(Product cl) throws IllegalAccessException {
        for(Validator v: validators)
        {
            if(v.validate(cl)==-1)
            {
                return -1;
            }
        }
        productDAO.insert(cl);
        return 0;
    }
    /**
     * This method deletes from the Product table that product whose id is given as a parameter.
     * @param id
     */
    public void delete(int id)
    {
        productDAO.delete(id);
    }

    /**
     *  This method updates the a product's info from  the Product table whose id is given as
     *  a parameter.
     * @param id-id of the product
     * @param c- the product
     * @return -1 if the product's info to be updated is not valid, 0 if it is
     * @throws IllegalAccessException
     */
    public int update(int id,Product c) throws IllegalAccessException {
        for(Validator v: validators)
        {
            if(v.validate(c)==-1)
            {
                return -1;
            }
        }
        productDAO.update(id,c);
        return 0;
    }
}
