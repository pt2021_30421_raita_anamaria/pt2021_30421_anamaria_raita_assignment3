package bll;

import java.util.ArrayList;
import java.util.List;
import java.util.NoSuchElementException;

import bll.validator.EmailValidator;
import bll.validator.NameValidator;
import bll.validator.StudentAgeValidator;
import bll.validator.Validator;
import dao.ClientDAO;
import model.Client;

/**
 * This class is used in order to implement all the business logic on a client.
 * It contains all the validations methods for the client.
 * It also contains all the operations we can perform on a client:insert,delete,update and show all.
 */
public class ClientBLL {

    private List<Validator<Client>> validators;
    private ClientDAO clientDAO;
    /**
     * @constructor
     * This constructor initialises the clientDao and the list of validators.
     */
    public ClientBLL() {
        validators = new ArrayList<Validator<Client>>();
        validators.add(new EmailValidator());
        validators.add(new StudentAgeValidator());
        validators.add(new NameValidator());
        clientDAO = new ClientDAO();
    }
    /**
     * This method returns the client with a certain id found in the client table.
     * @param id-the id of the client
     * @return a client
     */
    public Client findStudentById(int id) {
        Client st = clientDAO.findById(id);
        if (st == null) {
            throw new NoSuchElementException("The client with id =" + id + " was not found!");
        }
        return st;
    }
    /**
     * This method returns a list with all the clients in the table
     * @return List<Client>
     */
    public List<Client> findAll() {
        List<Client> clients= new ArrayList<>();
        clients = clientDAO.findAll();
        if (clients == null) {
            throw new NoSuchElementException("No clients to show");
        }
        return clients;
    }
    /**
     * This method is used in order to introduce a client,which is given as
     * a parameter in the data base if
     * all the info about it is valid.
     * @param cl-a client
     * @return -1 if the client's info is not valid, 0 if it is
     * @throws IllegalAccessException
     */
    public int insert(Client cl) throws IllegalAccessException {
        for(Validator v: validators)
        {
            if(v.validate(cl)==-1)
            {
                return -1;
            }
        }
        clientDAO.insert(cl);
        return 0;
    }

    /**
     * This method deletes from the Client table that client whose id is given as a parameter.
     * @param id
     */
    public void delete(int id)
    {
        clientDAO.delete(id);
    }
    /**
     *  This method updates the a client's info from  the Client table whose id is given as
     *  a parameter.
     * @param id-id of the client
     * @param c- the client
     * @return -1 if the product's info to be updated is not valid, 0 if it is
     * @throws IllegalAccessException
     */
    public int update(int id,Client c) throws IllegalAccessException {
        for(Validator v: validators)
        {
            if(v.validate(c)==-1)
            {
                return -1;
            }
        }
        clientDAO.update(id,c);
        return 0;
    }
}
