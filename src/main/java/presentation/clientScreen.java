package presentation;

import bll.ClientBLL;
import model.Client;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import dao.ClientDAO;
public class clientScreen {
    public JPanel panel;
    private JButton deleteButton;
    private JTextField textField1;
    private JTextField textField2;
    private JTextField textField3;
    private JTextField textField4;
    private JButton insertButton;
    private JButton updateButton;
    private JButton showAllButton;
    private JButton backButton;
    private JScrollPane sp;
    private JTable table1;
    private ClientDAO clientDAO=new ClientDAO();

    /**
     * This class implements the listeners for the 4 possible operations.
     * Insert and Update firstly check if the input is valid, then perform operations.
     * Show All inserts a JTable in a JScrollPane
     * And delete gets as parameter the id of the client to be deleted.
     * Also, here there is a back button which takes us back to the main screen.
     * @param jframe
     */
    public clientScreen(JFrame jframe){
        backButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                jframe.dispose();
                JFrame jFrame = new JFrame("welcomingScreen");
                jFrame.setContentPane(new welcomingScreen(jFrame).main);
                jFrame.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
                jFrame.pack();
                jFrame.setSize(1300,800);
                jFrame.setLocationRelativeTo(null);
                jFrame.setVisible(true);
            }
        });
        deleteButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                ClientBLL clientBLL=new ClientBLL();
                clientBLL.delete(Integer.valueOf(textField1.getText()));
            }
        });
        insertButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                ClientBLL clientBLL=new ClientBLL();
                Client client1 =new Client (textField2.getText(),textField3.getText(),Integer.valueOf(textField4.getText()));
                try {
                    if(clientBLL.insert(client1)==-1)
                        JOptionPane.showMessageDialog(null, "The client could not be inserted");
                    else
                        JOptionPane.showMessageDialog(null, "The record was inserted successfully");
                } catch (IllegalAccessException illegalAccessException) {
                    illegalAccessException.printStackTrace();
                }
            }
        });
        updateButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                ClientBLL clientBLL=new ClientBLL();
                Client client1 =new Client (textField2.getText(),textField3.getText(),Integer.valueOf(textField4.getText()));
                try {
                    if(clientBLL.update(Integer.valueOf(textField1.getText()),client1)==-1)
                        JOptionPane.showMessageDialog(null, "The client could not be updated");
                    else
                        JOptionPane.showMessageDialog(null, "The record was updated successfully");
                } catch (IllegalAccessException illegalAccessException) {
                    illegalAccessException.printStackTrace();
                }
            }
        });
        showAllButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                table1=clientDAO.createTable((ArrayList<?>) clientDAO.findAll());
                sp.setViewportView(table1);
                sp.setVisible(true);
            }
        });
    }
}
