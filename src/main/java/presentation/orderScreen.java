package presentation;

import bll.OrderBLL;
import dao.ClientDAO;
import dao.OrderDAO;
import dao.ProductDAO;
import model.Order;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;

public class orderScreen {
    public JPanel main;
    private JTextField textField1;
    private JButton insertButton;
    private JTextField textField2;
    private JTextField textField3;
    private JButton showAllButton;
    private JTextField textField4;
    private JButton generateBillButton;
    private JButton backButton;
    private JTable table1;
    private JScrollPane sp;
    private OrderDAO orderDAO =new OrderDAO();
    /**
     * This class implements the listeners for the 3 possible operations.
     * Insert  firstly checks if the input is valid, then perform operations.
     * Show All inserts a JTable in a JScrollPane
     * The make bill button generates the bills in a text file
     * Also, here there is a back button which takes us back to the main screen.
     * @param jframe
     */
    public orderScreen(JFrame jframe){

        backButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                jframe.dispose();
                JFrame jFrame = new JFrame("welcomingScreen");
                jFrame.setContentPane(new welcomingScreen(jFrame).main);
                jFrame.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
                jFrame.pack();
                jFrame.setSize(1300,800);
                jFrame.setLocationRelativeTo(null);
                jFrame.setVisible(true);
            }
        });
        insertButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                OrderBLL clientBLL=new OrderBLL();
                Order client1 =new Order (Integer.valueOf(textField3.getText()),Integer.valueOf(textField2.getText()),Integer.valueOf(textField4.getText()));
                try {
                    if(clientBLL.insert(client1)==-1)
                        JOptionPane.showMessageDialog(null, "The order could not be inserted");
                    else
                        JOptionPane.showMessageDialog(null, "The record was inserted successfully");
                } catch (IllegalAccessException illegalAccessException) {
                    illegalAccessException.printStackTrace();
                }
            }
        });
        showAllButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                table1= orderDAO.createTable((ArrayList<?>) orderDAO.findAll());
                sp.setViewportView(table1);
                sp.setVisible(true);
            }
        });
        generateBillButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                ProductDAO productDAO= new ProductDAO();
                ClientDAO clientDAO=new ClientDAO();
                ArrayList<Order> orders= (ArrayList<Order>) orderDAO.findAll();
                for(Order ord: orders)
                {   String path="Bill"+ord.getId()+".txt";
                    createFile(path);
                    String output="";
                    output=output+"Name: "+clientDAO.findById(ord.getClient_id()).getName()+"\n";
                    output=output+"Product name: "+productDAO.findById(ord.getProduct_id()).getName()+"\n";
                    output=output+"Product price: "+productDAO.findById(ord.getProduct_id()).getPrice()+"\n";
                    output=output+"Quantity: "+ord.getQuantity()+"\n";
                    output=output+"Total Price: "+productDAO.findById(ord.getProduct_id()).getPrice()*ord.getQuantity()+"\n";
                    writeFile(path,output);
                }
                JOptionPane.showMessageDialog(null,"The bills were generated successfully!");
            }
        });
    }
    /**
     * @method writeFile
     * it writes in a file
     * @param s
     * @param content
     */
    public void writeFile(String s,String content)
    {
        try {
            FileWriter myWriter = new FileWriter(s);
            myWriter.write(content);
            myWriter.close();
        } catch (IOException e) {
        }
    }
    /**
     * @method createFile
     * it creates a file
     * @param s
     */
    public void createFile(String s)
    {
        try {
            File myObj = new File(s);
            myObj.createNewFile();
        } catch (IOException e) {

        }
    }
}
