package presentation;

import bll.ClientBLL;
import bll.ProductBLL;
import dao.ClientDAO;
import dao.ProductDAO;
import model.Client;
import model.Product;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

public class productScreen {
    public JPanel main;
    private JTextField textField1;
    private JButton deleteButton;
    private JTextField textField2;
    private JButton insertButton;
    private JTextField textField3;
    private JButton editButton;
    private JTextField textField4;
    private JButton showAllButton;
    private JButton backButton;
    private JScrollPane sp;
    private JTable table1;
    private ProductDAO clientDAO=new ProductDAO();
    /**
     * This class implements the listeners for the 4 possible operations.
     * Insert and Update firstly check if the input is valid, then perform operations.
     * Show All inserts a JTable in a JScrollPane
     * And delete gets as parameter the id of the product to be deleted.
     * Also, here there is a back button which takes us back to the main screen.
     * @param jframe
     */
    public productScreen(JFrame jframe){

        backButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                jframe.dispose();
                JFrame jFrame = new JFrame("welcomingScreen");
                jFrame.setContentPane(new welcomingScreen(jFrame).main);
                jFrame.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
                jFrame.pack();
                jFrame.setSize(1300,800);
                jFrame.setLocationRelativeTo(null);
                jFrame.setVisible(true);
            }
        });
        deleteButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
               ProductBLL clientBLL=new ProductBLL();
                clientBLL.delete(Integer.valueOf(textField1.getText()));
            }
        });
        insertButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                ProductBLL clientBLL=new ProductBLL();
                Product client1 =new Product (textField2.getText(),Integer.valueOf(textField3.getText()),Integer.valueOf(textField4.getText()));
                try {
                    if(clientBLL.insert(client1)==-1)
                        JOptionPane.showMessageDialog(null, "The product could not be inserted");
                    else
                        JOptionPane.showMessageDialog(null, "The record was inserted successfully");
                } catch (IllegalAccessException illegalAccessException) {
                    illegalAccessException.printStackTrace();
                }
            }
        });
        editButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                ProductBLL clientBLL=new ProductBLL();
                Product client1 =new Product (textField2.getText(),Integer.valueOf(textField3.getText()),Integer.valueOf(textField4.getText()));
                try {
                    if(clientBLL.update(Integer.valueOf(textField1.getText()),client1)==-1)
                        JOptionPane.showMessageDialog(null, "The product could not be inserted");
                    else
                        JOptionPane.showMessageDialog(null, "The record was inserted successfully");
                } catch (IllegalAccessException illegalAccessException) {
                    illegalAccessException.printStackTrace();
                }
            }
        });
        showAllButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                table1=clientDAO.createTable((ArrayList<?>) clientDAO.findAll());
                sp.setViewportView(table1);
                sp.setVisible(true);
            }
        });
    }
}
