package presentation;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

/**
 * This class is used to implement the first screen of the GUI
 * where the user can choose on what object he/she wants to perform operations.
 */
public class welcomingScreen {
    public JPanel main;
    private JButton orderOperationButton;
    private JButton clientOperationButton;
    private JButton productOperationButton;

    /**
     * This method is used to start the application.
     * @param args
     */
    public static void main(String[] args) {
        JFrame frame = new JFrame("welcomingScreen");
        frame.setContentPane(new welcomingScreen(frame).main);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.pack();
        frame.setVisible(true);
        JOptionPane.showMessageDialog(null, "Introduce the name only with letters.\n" +
                "Introduce a valid e-mail.\nThe quantities and price should be positive\n Age between 14 and 100.\n Order quantity should be less than product quantity.");

    }

    /**
     * This method is used in order to create the frame in which all the components are put.
     * It contains the listeners for all 3 buttons:Client,Order, Product.
     * Each button opens a new frame which allows us to perform operations.
     * * @param frame
     */
    public welcomingScreen(JFrame frame) {
        clientOperationButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                frame.dispose();
                JFrame jFrame = new JFrame("Client");
                jFrame.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
                jFrame.setContentPane(new clientScreen(jFrame).panel);
                jFrame.pack();
                jFrame.setSize(1300,800);
                jFrame.setLocationRelativeTo(null);
                jFrame.setVisible(true);
            }
        });
        productOperationButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                frame.dispose();
                JFrame jFrame = new JFrame("Product");
                jFrame.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
                jFrame.setContentPane(new productScreen(jFrame).main);
                jFrame.pack();
                jFrame.setSize(1300,800);
                jFrame.setLocationRelativeTo(null);
                jFrame.setVisible(true);
            }
        });
        orderOperationButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                frame.dispose();
                JFrame jFrame = new JFrame("Order");
                jFrame.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
                jFrame.setContentPane(new orderScreen(jFrame).main);
                jFrame.pack();
                jFrame.setSize(1300,800);
                jFrame.setLocationRelativeTo(null);
                jFrame.setVisible(true);
            }
        });
    }
}
