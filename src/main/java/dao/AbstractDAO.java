package dao;

import java.beans.IntrospectionException;
import java.beans.PropertyDescriptor;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.lang.reflect.ParameterizedType;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import connection.ConnectionFactory;

import javax.swing.*;
import javax.swing.table.DefaultTableModel;

/**
 *
 * This class implements the methods that make the queries through reflection techniques
 */
public class AbstractDAO<T> {
    protected static final Logger LOGGER = Logger.getLogger(AbstractDAO.class.getName());

    private final Class<T> type;
    /**
     * @constructor
     * This initializes the type field
     */
    @SuppressWarnings("unchecked")
    public AbstractDAO() {
        this.type = (Class<T>) ((ParameterizedType) getClass().getGenericSuperclass()).getActualTypeArguments()[0];

    }
    /**
     *  createSelectQuery
     * This method creates the query for finding an item  by id
     * @param field
     * @return -String which represents the result of the query
     */
    private String createSelectQuery(String field) {
        StringBuilder sb = new StringBuilder();
        sb.append("SELECT ");
        sb.append(" * ");
        sb.append(" FROM ");
        sb.append(type.getSimpleName());
        sb.append(" WHERE " + field + " =?");
        return sb.toString();
    }

    /**
     *
     * This method executes to query to select the item from the table
     * whose is given as a parameter
     * @param id- id of what we want to find
     * @return null;
     */
    public T findById(int id) {
        Connection connection = null;
        PreparedStatement statement = null;
        ResultSet resultSet = null;
        String query = createSelectQuery("id");
        try {
            connection = ConnectionFactory.getConnection();
            statement = connection.prepareStatement(query);
            statement.setInt(1, id);
            resultSet = statement.executeQuery();

            return createObjects(resultSet).get(0);
        } catch (SQLException e) {
            LOGGER.log(Level.WARNING, type.getName() + "DAO:findById " + e.getMessage());
        } finally {
            ConnectionFactory.close(resultSet);
            ConnectionFactory.close(statement);
            ConnectionFactory.close(connection);
        }
        return null;
    }
    /**
     *
     * This method returns the list of objects created from the ResultSet
     * @param resultSet- what set results by executing a query
     * @return  null
     */
    private List<T> createObjects(ResultSet resultSet) {
        List<T> list = new ArrayList<T>();

        try {
            while (resultSet.next()) {
                T instance = type.newInstance();
                for (Field field : type.getDeclaredFields()) {
                    Object value = resultSet.getObject(field.getName());
                    PropertyDescriptor propertyDescriptor = new PropertyDescriptor(field.getName(), type);
                    Method method = propertyDescriptor.getWriteMethod();
                    method.invoke(instance, value);
                }
                list.add(instance);
            }
        } catch (InstantiationException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        } catch (SecurityException e) {
            e.printStackTrace();
        } catch (IllegalArgumentException e) {
            e.printStackTrace();
        } catch (InvocationTargetException e) {
            e.printStackTrace();
        } catch (SQLException e) {
            e.printStackTrace();
        } catch (IntrospectionException e) {
            e.printStackTrace();
        }
        return list;
    }
    /**
     *
     *This method creates the query for inserting an item
     * @param t
     * @return String which represents the query itself
     * @throws IllegalAccessException
     */
    public String insertQuery(T t)throws IllegalAccessException
    {
        StringBuilder sb= new StringBuilder();
        StringBuilder aux=new StringBuilder();
        sb.append("INSERT ");
        sb.append("INTO ");
        sb.append("javaDatabase."+type.getSimpleName()+"(");
        for(Field f:t.getClass().getDeclaredFields()){
        }
        sb.delete(sb.length()-2,sb.length());
        aux.delete(aux.length()-2, aux.length());
        sb.append(")");
        sb.append(" VALUES(").append(aux).append(")");
        return sb.toString();
    }
    /**
     *
     *This method creates the query for deleting an item wtih a certain id
     * @param id
     * @return String which represents the query itself
     * @throws IllegalAccessException
     */
    public String deleteQuery(int id)
    {
        StringBuilder sb= new StringBuilder();

        sb.append("DELETE FROM ");
        sb.append("javaDatabase."+type.getSimpleName());
        sb.append(" WHERE ID=");
        sb.append(id);

        return sb.toString();
    }

    /**
     *
     *This method creates the query for editing an item
     * @param t
     * @return String which represents the query itself
     * @throws IllegalAccessException
     */
    public String editQuery(int id,T t) throws IllegalAccessException
    {
        StringBuilder sb= new StringBuilder();
        sb.append("UPDATE ");
        sb.append("javaDatabase."+type.getSimpleName());
        sb.append(" SET ");
        ArrayList<Field> f=new ArrayList<>();
        f.addAll(Arrays.asList(t.getClass().getDeclaredFields()));
        for(int i = 1; i< t.getClass().getDeclaredFields().length; i++)
        {
        }
        sb.delete(sb.length()-2,sb.length());
        sb.append(" WHERE ID=");
        sb.append(id);
        return sb.toString();
    }

    /**
     *
     *This method creates the query for showing all items in a table
     * @return String which represents the query itself
     * @throws IllegalAccessException
     */
    private String showAllQuery() {
        StringBuilder sb = new StringBuilder();
        sb.append("SELECT ");
        sb.append(" * ");
        sb.append(" FROM ");
        sb.append("javaDatabase."+type.getSimpleName());
        return sb.toString();
    }
    /**
     *
     * This method executes the insert query
     * @param t
     * @throws IllegalAccessException
     */
    public void insert(T t) throws IllegalAccessException {
        Connection connection = null;
        PreparedStatement statement = null;
        String query = insertQuery(t);
        try {
            connection = ConnectionFactory.getConnection();
            statement = connection.prepareStatement(query);
            statement.executeUpdate();

        } catch (SQLException e) {
            LOGGER.log(Level.WARNING, type.getName() + "DAO:Insert " + e.getMessage());
        } finally {
            ConnectionFactory.close(statement);
            ConnectionFactory.close(connection);
        }

    }
    /**
     *
     * This method executes the update query
     * @param id whose element we want to modify
     * @param t
     * @throws IllegalAccessException
     */
    public void update(int id, T t) throws IllegalAccessException {
        Connection connection = null;
        PreparedStatement statement = null;
        String sb = editQuery(id, t);
        try {
            connection = ConnectionFactory.getConnection();
            statement = connection.prepareStatement(sb);
            statement.executeUpdate();

        } catch (SQLException e) {
            LOGGER.log(Level.WARNING, type.getName() + "DAO:Update " + e.getMessage());
        } finally {
            ConnectionFactory.close(statement);
            ConnectionFactory.close(connection);
        }
    }
    /**
     *
     * This method executes the delete query
     * @param id whose element we want to delete
     */
    public void delete(int id) {
        Connection connection = null;
        PreparedStatement statement = null;
        String sb=deleteQuery(id);
        try {
            connection = ConnectionFactory.getConnection();
            statement = connection.prepareStatement(sb);
            statement.executeUpdate();

        } catch (SQLException e) {
            LOGGER.log(Level.WARNING, type.getName() + "DAO:Delete " + e.getMessage());
        } finally {
            ConnectionFactory.close(statement);
            ConnectionFactory.close(connection);
        }
    }
    /**
     *
     * This method executes the query to select all the items from the table
     * @return List<T>
     */
    public List<T> findAll() {
        Connection connection = null;
        PreparedStatement statement = null;
        ResultSet resultSet = null;
        String query =showAllQuery();
        try {
            connection = ConnectionFactory.getConnection();
            statement = connection.prepareStatement(query);
            resultSet = statement.executeQuery();
            return createObjects(resultSet);
        } catch (SQLException e) {
            LOGGER.log(Level.WARNING, type.getName() + "DAO:findAll " + e.getMessage());
        } finally {
            ConnectionFactory.close(resultSet);
            ConnectionFactory.close(statement);
            ConnectionFactory.close(connection);
        }
        return null;
    }

    /**
     * This method creates a table from a list of objects given as parameter
     * @param myList
     * @return Jtable, a table respecting the structure in the database
     */

}