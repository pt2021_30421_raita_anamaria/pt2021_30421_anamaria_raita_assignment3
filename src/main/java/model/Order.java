package model;
/**
 * This class is used to model the data about the client from the table Order in MYSQL.
 * It has 4 attributes:id, client_id, product_id, quantity.
 * It contains setters and getters for all the attributes.
 * It has a default constructor and one other constructors.
 * It also has a toString method in order to print the order with all the attributes.
 */
public class Order {

    private int client_id;
    private int id;
    private int product_id;
    private int quantity;
    /**
     *
     * default constructor
     */
    public Order(){

    }
    /**
     * getter for client id
     * @return id
     */
    public int getClient_id() {
        return client_id;
    }

    /**
     * getter for order id
     * @return id
     */
    public int getId() {
        return id;
    }

    /**
     * setter for order id
     * @param id
     */

    public void setId(int id) {
        this.id = id;
    }
    /**
     * getter for product id
     * @return id
     */

    public int getProduct_id() {
        return product_id;
    }

    /**
     * getter for quantity of order
     * @return int, quantity of order
     */
    public int getQuantity() {
        return quantity;
    }

    /**
     * setter for the quantity of order
     * @param quantity
     */

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

    /**
     * it builds the object Order
     * @param client_id
     * @param product_id
     * @param quantity
     */
    public Order(int client_id,int product_id, int quantity) {
        super();
        this.client_id = client_id;
        this.product_id = product_id;
        this.quantity = quantity;
    }
    /**
     * this method is used to print the order
     * @return a string which represents the info of the order
     */

    @Override
    public String toString() {
        return "Order[" +
                "client_id=" + client_id +
                ", order_id=" + id +
                ", product_id=" + product_id +
                ", quantity=" + quantity +
                ']';
    }
}
