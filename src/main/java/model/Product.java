package model;
/**
 * This class is used to model the data about the client from the table Product in MYSQL.
 * It has 4 attributes:id, name, price, quantity.
 * It contains setters and getters for all the attributes.
 * It has a default constructor and one other constructors.
 * It also has a toString method in order to print the product with all the attributes.
 */
public class Product {
    private int id;
    private String name;
    private float price;
    private int quantity;

    /**
     * it build the object product
     * @param name
     * @param price
     * @param quantity
     */
    public Product( String name, float price, int quantity) {
        super();
        this.name = name;
        this.price = price;
        this.quantity = quantity;
    }
    /**
     *
     * default constructor
     */
public Product(){

}
    /**
     * getter for product id
     * @return id
     */
    public int getId() {
        return id;
    }
    /**
     * setter for product id
     * @param id
     */

    public void setId(int id) {
        this.id = id;
    }
    /**
     * getter for product name
     * @return String, the name of the product
     */

    public String getName() {
        return name;
    }

    /**
     * setter for product name
     * @param name
     */
    public void setName(String name) {
        this.name = name;
    }
    /**
     * getter for product price
     * @return price
     */

    public float getPrice() {
        return price;
    }
    /**
     * setter for product price
     * @param price
     */

    public void setPrice(float price) {
        this.price = price;
    }
    /**
     * getter for product quantity
     * @return int, quantity of product
     */
    public int getQuantity() {
        return quantity;
    }
    /**
     * setter for product quantity
     * @param quantity
     */

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }
}
