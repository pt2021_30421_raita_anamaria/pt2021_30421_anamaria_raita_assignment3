package model;

/**
 * This class is used to model the data about the client from the table Client in MYSQL.
 * It has 4 attributes:id, name, e-mail, age.
 * It contains setters and getters for all the attributes.
 * It has a default constructor and 2 other constructors.(one gets the id as a parameter,
 * one does not).
 * It also has a toString method in order to print the client with all the attributes.
 */
public class Client {
    private int id;
    private String name;
    private String email;
    private int age;

    /**
     *
     * default constructor
     */
    public Client() {
    }

    /**
     * the constructor for object Client
     * @param id
     * @param name
     * @param email
     * @param age
     */
    public Client(int id, String name,String email, int age) {
        super();
        this.id = id;
        this.name = name;
        this.email = email;
        this.age = age;
    }

    /**
     * The constructor for client object
     * @param name
     * @param email
     * @param age
     */
    public Client(String name, String email, int age) {
        super();
        this.name = name;
        this.email = email;
        this.age = age;
    }

    /**
     * getter for id
     * @return id
     */
    public int getId() {
        return id;
    }

    /**
     * setter for id
     * @param id
     */
    public void setId(int id) {
        this.id = id;
    }

    /**
     * getter for name
     * @return String which is name of the client
     */
    public String getName() {
        return name;
    }

    /**
     * setter for the name
     * @param name
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     *  getter for age of the client
     * @return int, age of the client
     */
    public int getAge() {
        return age;
    }

    /**
     * setter for the age of the client
     * @param age
     */
    public void setAge(int age) {
        this.age = age;
    }

    /**
     * getter for the e-mail
     * @return String, which is the e-mail of the client
     */
    public String getEmail() {
        return email;
    }

    /**
     * this method is used to print the client
     * @return a string which represents the info of the client
     */

    @Override
    public String toString() {
        return "Client[id=" + id + ", name=" + name + ", email=" + email + ", age=" + age
                + "]";
    }

}