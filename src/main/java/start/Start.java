package start;

import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;

import bll.ClientBLL;
import dao.ClientDAO;
import model.Client;
import presentation.welcomingScreen;

public class Start {
    protected static final Logger LOGGER = Logger.getLogger(Start.class.getName());
    public static void main(String[] args) throws SQLException {

        ClientBLL studentBll = new ClientBLL();
        ClientDAO dao=new ClientDAO();
        Client client1 =new Client ("xlin","xlin@yahoo.com",15);

        try {
            studentBll.insert(client1);

        } catch (Exception ex) {
            ex.printStackTrace();
        }

        // obtain field-value pairs for object through reflection
        ReflectionExample.retrieveProperties(client1);

    }

}
